import { setDefaultOptions, loadModules } from 'esri-loader';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-drawmap',
  templateUrl: './drawmap.component.html',
  styleUrls: ['./drawmap.component.css']
})
export class DrawmapComponent implements OnInit {
  @ViewChild('mapElement')
  private mapElement: ElementRef<HTMLDivElement>
  map:any
  constructor() { }

  ngOnInit(): void {
    setDefaultOptions({ version: '3.31', css: true })
    this.createMap()
  }

  async createMap(): Promise<void> {
    const [Map, FeatureLayer] = await loadModules([
      'esri/map',
      "esri/layers/FeatureLayer"
    ])
    this.map = new Map(this.mapElement.nativeElement, {
      basemap: 'topo',
      center: [-100.456848, 31.442778,],
      zoom: 4
    
    })
    var parksLayer = new FeatureLayer("https://sampleserver1.arcgisonline.com/ArcGIS/rest/services/Demographics/ESRI_Census_USA/MapServer/5");
    this.map.addLayer(parksLayer)
 
    this.map.on('load', () => this.mapReady())
    this.map.on('click',(event) => this.addToMap(event))
  }

  async mapReady(){
  }

  async mapclick(){
  }
  async addToMap(event){
    const [Graphic,Draw,SimpleLineSymbol,SimpleFillSymbol,Color] = await loadModules([
      'esri/graphic',
      "esri/toolbars/draw",
      "esri/symbols/SimpleLineSymbol",
      "esri/symbols/SimpleFillSymbol",
      "esri/Color"
    ])

   
    var toolbar = new Draw(this.map);
    toolbar.on("draw-end", this.addToMap);
    var lineSymbol = new SimpleLineSymbol(SimpleLineSymbol.STYLE_DASH,new Color([255,0,0]),3);
    var sfs = new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID,lineSymbol,new Color([255,0,0]));
    var graphic = new Graphic(event.mapPoint, sfs);
    console.log(event.mapPoint)
    this.map.graphics.add(graphic);
  }

}