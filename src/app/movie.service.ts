import { Movie } from './Movie';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';



@Injectable({
  providedIn: 'root'
})
export class MovieService {
  postsArray:[]

  constructor(private http: HttpClient) { }

  getMovie(): Observable<Movie[]> {
    return this.http.get<Movie[]>('../assets/test.json')
  }

  getMovieDetail(): Observable<any> {
      return this.http.get('../assets/test.json');

}
}
