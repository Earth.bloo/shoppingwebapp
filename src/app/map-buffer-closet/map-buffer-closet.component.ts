import { setDefaultOptions, loadModules } from 'esri-loader';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-map-buffer-closet',
  templateUrl: './map-buffer-closet.component.html',
  styleUrls: ['./map-buffer-closet.component.css']
})
export class MapBufferClosetComponent implements OnInit {
  @ViewChild('mapElement')
  private mapElement: ElementRef<HTMLDivElement>
  map: any = null
  mappoint:any
  buffer
  constructor() { }

  ngOnInit(): void {
    setDefaultOptions({ version: '3.31', css: true })
    this.createMap()
  }
  
  async createMap(): Promise<void> {
    const [Map, FeatureLayer,geometryEngine] = await loadModules([
      'esri/map',
      "esri/layers/FeatureLayer",
      "esri/geometry/geometryEngine"
    ])
    this.map = new Map(this.mapElement.nativeElement, {
      basemap: 'topo',
      center: [-122.45, 37.75],
      zoom: 13
      
    })
    
    this.map.on('load', () => this.mapReady())
    this.map.on('click',(event) => this.clickMap(event))

  }

  async clickMap(event){ 
    const [Graphic,SimpleMarkerSymbol,geometryEngine,SimpleFillSymbol,SimpleLineSymbol,Color] = await loadModules([
    'esri/graphic',
    "esri/symbols/SimpleMarkerSymbol",
    "esri/geometry/geometryEngine",
    "esri/symbols/SimpleFillSymbol",
    "esri/symbols/SimpleLineSymbol",
    "esri/Color"
    
  ])
    this.map.graphics.clear()
    this.mappoint = event.mapPoint
    this.buffer = geometryEngine.geodesicBuffer(this.mappoint, 10, "kilometers")
    var lineSymbol = new SimpleLineSymbol(SimpleLineSymbol.STYLE_DASH,new Color([255,0,0,0.4]),3);
    var sfs = new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID,lineSymbol,new Color([255,0,0,0.4]));
    var graphicBuffer = new Graphic(this.buffer,sfs)

    var pointGrahic = new SimpleMarkerSymbol()
    var point =  new Graphic(this.mappoint,pointGrahic)
    this.map.graphics.add(graphicBuffer)
    this.map.graphics.add(point)
    this.filterlocation()
  }
  async filterlocation(){
    const [Query,QueryTask] = await loadModules([
      "esri/tasks/query",
      "esri/tasks/QueryTask"
    ])

    var query = new Query()
    query.geometry = this.buffer
    var queryTask = new QueryTask("https://sampleserver1.arcgisonline.com/ArcGIS/rest/services/Specialty/ESRI_StatesCitiesRivers_USA/MapServer/0");
    query.returnGeometry = true
    query.outSpatialReference = this.map.spatialReference
    queryTask.execute(query,(result) => {
      console.log(result)
    });

    

  }

  async mapReady(){
    const [FeatureLayer] = await loadModules([
      "esri/layers/FeatureLayer"
    ])
    var parksLayer = new FeatureLayer("https://sampleserver1.arcgisonline.com/ArcGIS/rest/services/Specialty/ESRI_StatesCitiesRivers_USA/MapServer/0");
    this.map.addLayer(parksLayer)
    
  }


}
