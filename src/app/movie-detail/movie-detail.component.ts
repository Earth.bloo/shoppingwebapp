import { ActivatedRoute } from '@angular/router';
import { MovieService } from './../movie.service';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { loadModules, setDefaultOptions } from 'esri-loader'
import { Movie } from '../Movie';

@Component({
  selector: 'app-movie-detail',
  templateUrl: './movie-detail.component.html',
  styleUrls: ['./movie-detail.component.css']
})
export class MovieDetailComponent implements OnInit {
  @ViewChild('mapElement')
  private mapElement: ElementRef<HTMLDivElement>

  map: any = null
  movieDetail:any

  movie?:Movie
  id:string
  constructor(private movieService: MovieService,private router:ActivatedRoute) { 
   
   }

  ngOnInit(): void {
    setDefaultOptions({ version: '3.31', css: true })
     this.id = this.router.snapshot.params.id

     this.movieService.getMovie().subscribe((result) => {
        this.setMovie(result[this.id])
    })
    this.createMap()
    this.loadMovieDetail()
  }

  async createMap(): Promise<void> {
    const [Map, FeatureLayer] = await loadModules([
      'esri/map',
      "esri/layers/FeatureLayer"
    ])
    this.map = new Map(this.mapElement.nativeElement, {
      basemap: 'topo',
      center: [100.0, 13.0],
      zoom: 3
    
    })
    var parksLayer = new FeatureLayer("https://services3.arcgis.com/GVgbJbqm8hXASVYi/arcgis/rest/services/Parks_and_Open_Space/FeatureServer/0");
    this.map.addLayer(parksLayer)
    var trailheadsLayer = new FeatureLayer("https://services3.arcgis.com/GVgbJbqm8hXASVYi/arcgis/rest/services/Trailheads/FeatureServer/0")
    this.map.addLayer(trailheadsLayer);
    this.map.on('load', () => this.mapReady())
  }
  mapReady(): void {
    this.createGraphic()
  }


  async createGraphic(){
    const [Graphic, SimpleMarkerSymbol, Point,Color,SimpleLineSymbol] = await loadModules([
      'esri/graphic',
      'esri/symbols/SimpleMarkerSymbol',
      'esri/geometry/Point',
      "esri/Color",
      "esri/symbols/SimpleLineSymbol"
    ])
    var sms = new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_SQUARE, 10,
      new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
      new Color([255,0,0]), 1),
      new Color([0,255,0,0.25]));
      const point = new Point(100.0, 13.0)
      var graphic =  new Graphic(point, sms)
      //console.log(graphic)
      this.map.graphics.add(graphic)

  }


  loadMovieDetail(){
    this.movieService.getMovieDetail().subscribe((result) => {
      console.log(result);
      this.movieDetail = result
    });
  }


  setMovie(result:Movie){
    this.movie = result

  }


}
