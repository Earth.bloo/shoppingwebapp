import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TestPageComponent } from './test-page/test-page.component';
import { SecondComponent } from './second/second.component';
import { FirstComponent } from './first/first.component';
import { MovieDetailComponent } from './movie-detail/movie-detail.component';
import { HttpClientModule } from '@angular/common/http';
import { MapComponent } from './map/map.component';
import { MapBufferClosetComponent } from './map-buffer-closet/map-buffer-closet.component';
import { DrawmapComponent } from './drawmap/drawmap.component';

@NgModule({
  declarations: [
    AppComponent,
    TestPageComponent,
    SecondComponent,
    FirstComponent,
    MovieDetailComponent,
    MapComponent,
    MapBufferClosetComponent,
    DrawmapComponent,
   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
