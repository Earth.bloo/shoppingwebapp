import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { loadModules, setDefaultOptions } from 'esri-loader'

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {
  @ViewChild('mapElement')
  private mapElement: ElementRef<HTMLDivElement>

  map: any = null
  arrayFeature:any


  constructor() { 
    
  }

  ngOnInit(): void {
    setDefaultOptions({ version: '3.31', css: true })
    this.createMap()
  }

  async createMap(): Promise<void> {
    const [Map, FeatureLayer] = await loadModules([
      'esri/map',
      "esri/layers/FeatureLayer"
    ])
    this.map = new Map(this.mapElement.nativeElement, {
      basemap: 'topo',
      center: [-100.456848, 31.442778,],
      zoom: 4
    
    })
    var parksLayer = new FeatureLayer("https://sampleserver1.arcgisonline.com/ArcGIS/rest/services/Demographics/ESRI_Census_USA/MapServer/5");
    this.map.addLayer(parksLayer)
 
    this.map.on('load', () => this.mapReady())
  }

  async mapReady(){
    const [Query, QueryTask,Lang] = await loadModules([
      "esri/tasks/query", 
      "esri/tasks/QueryTask",
      "esri/lang"
    ])

   
    var query = new Query();
    var queryTask = new QueryTask("https://sampleserver1.arcgisonline.com/ArcGIS/rest/services/Demographics/ESRI_Census_USA/MapServer/5");
    query.where = "1=1"; 
    query.returnGeometry = true
    query.outSpatialReference = this.map.spatialReference
    query.outFields = ["SUB_REGION","STATE_NAME","STATE_ABBR"];
    queryTask.execute(query,(result) => {
      this.showData(result)
    });
  }
  showData(result:any){
   this.arrayFeature = result.features
   console.log(this.arrayFeature)
    
    
  }

  async clickFeature(feature){
    const [Graphic, Point,Color,SimpleLineSymbol,SimpleFillSymbol] = await loadModules([
      'esri/graphic',
      'esri/geometry/Point',
      "esri/Color",
      "esri/symbols/SimpleLineSymbol",
      "esri/symbols/SimpleFillSymbol"
    ])
    this.map.graphics.clear()
    var lineSymbol = new SimpleLineSymbol(SimpleLineSymbol.STYLE_DASH,new Color([255,0,0]),3);
    var sfs = new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID,lineSymbol,new Color([255,0,0]));
    var graphic =  new Graphic(feature.geometry,sfs)
    this.map.graphics.add(graphic)


  }

}
