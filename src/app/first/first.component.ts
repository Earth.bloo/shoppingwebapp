import { Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import { MovieService } from '../movie.service';
import { Movie } from '../Movie';



@Component({
  selector: 'app-first',
  templateUrl: './first.component.html',
  styleUrls: ['./first.component.css']
})
export class FirstComponent implements OnInit {
  movies:Movie[]

  constructor(private movieService: MovieService) { 
    this.movieService.getMovie().subscribe((result) => {
       this.setMovie(result)
    })
  }

  ngOnInit(): void {
  }

  setMovie(result:Movie[]){
    this.movies = result
    console.log(this.movies)
  }

}
