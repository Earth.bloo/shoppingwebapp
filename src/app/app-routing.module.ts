import { DrawmapComponent } from './drawmap/drawmap.component';
import { MapBufferClosetComponent } from './map-buffer-closet/map-buffer-closet.component';
import { MapComponent } from './map/map.component';
import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TestPageComponent } from './test-page/test-page.component';
import { FirstComponent } from './first/first.component';
import { SecondComponent } from './second/second.component';
import { MovieDetailComponent } from './movie-detail/movie-detail.component';


const routes: Routes = [{
  path:'', component: TestPageComponent, children:[
    {path:'page-1', component: FirstComponent},
    {path:'page-2', component: SecondComponent},
    {path:'detail/:id',component: MovieDetailComponent}
  ]
},
{path:'map',component:MapComponent},
{path:'mapbuffercloset',component:MapBufferClosetComponent},
{path:'drawmap',component:DrawmapComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
